import PodcastInterface from "@/model/PodcastInterface";
import {Type} from "class-transformer";

export default class Podcast implements PodcastInterface {
  private id: number = 0;
  public title: string = '';
  public description: string = '';

  @Type(() => Date)
  public last_update_date: Date = new Date();
  public link_url: string = '';
  public image_url: string = '';
  public feed_url: string = '';

  getId(): number {
    return this.id;
  }

  getTitle(): string {
    return this.title;
  }

  setTitle(title: string): void {
    this.title = title;
  }

  getDescription(): string {
    return this.description;
  }

  setDescription(description: string): void {
    this.description = description;
  }

  getLastUpdateDate(): Date {
    return this.last_update_date;
  }

  setLastUpdateDate(lastUpdateDate: Date): void {
    this.last_update_date = lastUpdateDate;
  }

  getLinkUrl(): string {
    return this.link_url;
  }

  setLinkUrl(linkUrl: string): void {
    this.link_url = linkUrl;
  }

  getImageUrl(): string {
    return this.image_url;
  }

  setImageUrl(imageUrl: string): void {
    this.image_url = imageUrl;
  }

  getFeedUrl(): string {
    return this.feed_url;
  }

  setFeedUrl(feedUrl: string): void {
    this.feed_url = feedUrl;
  }
}
