import PodcastItemInterface from "@/model/PodcastItemInterface";
import {Type} from "class-transformer";

export default class PodcastItem implements PodcastItemInterface {
  private id: number = 0;
  public title: string = '';
  public description: string = '';

  @Type(() => Date)
  public pub_date: Date = new Date();
  public length: number = 0;
  public size: number = 0;
  public url: string = '';

  getId(): number {
    return this.id;
  }

  getTitle(): string {
    return this.title;
  }

  setTitle(title: string): void {
    this.title = title;
  }

  getDescription(): string {
    return this.description;
  }

  setDescription(description: string): void {
    this.description = description;
  }

  getPublishingDate(): Date {
    return this.pub_date;
  }

  setPublishingDate(publishingDate: Date): void {
    this.pub_date = publishingDate;
  }

  getUrl(): string {
    return this.url;
  }

  setUrl(url: string): void {
    this.url = url;
  }

  getLength(): number {
    return this.length;
  }

  setLength(length: number): void {
    this.length = length
  }

  getSize(): number {
    return this.size
  }

  setSize(size: number): void {
    this.size = size;
  }
}
