export default interface PodcastInterface {
  getId(): number;

  getTitle(): string;

  setTitle(title: string): void;

  getDescription(): string;

  setDescription(description: string): void;

  getLastUpdateDate(): Date;

  setLastUpdateDate(lastUpdateDate: Date): void;

  getLinkUrl(): string;

  setLinkUrl(linkUrl: string): void;

  getImageUrl(): string;

  setImageUrl(imageUrl: string): void;

  getFeedUrl(): string;

  setFeedUrl(feedUrl: string): void;
}
