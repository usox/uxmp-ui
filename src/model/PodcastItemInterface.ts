export default interface PodcastItemInterface {
  getId(): number;

  getTitle(): string;

  setTitle(title: string): void;

  getDescription(): string;

  setDescription(description: string): void;

  getPublishingDate(): Date;

  setPublishingDate(publishingDate: Date): void;

  getUrl(): string;

  setUrl(url: string): void;

  getLength(): number;

  setLength(length: number): void;

  getSize(): number;

  setSize(size: number): void;
}
